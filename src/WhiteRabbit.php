<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $myfile = file_get_contents($filePath);
        $str = preg_replace("/[^\w_]+/u", '', $myfile);
        $str = preg_replace("/[_\xC0-\xDF0-9+]/", "", $str);
        $str = strtolower($str);
        return $str;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $parsedArray = str_split($parsedFile);
        $parsedArray = array_count_values($parsedArray);
        asort($parsedArray);
        $medianPos = round(count($parsedArray) / 2) - 1;
        $parsedKeys = array_keys($parsedArray);
        $medianKey = $parsedKeys[$medianPos];
        $occurrences = $parsedArray[$medianKey];
        return $medianKey;
    }
}
?>